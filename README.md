![Build Status](https://gitlab.com/digitalsats/digitalsatswebpages/-/pipelines/)

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
<!-- **Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)* -->
**Table of Contents**

- [GitLab CI](#gitlab-ci)
- [Information about the Web Pages](#information-about-the-web-pages)
- [Example of How to Edit the Web Pages](#example-of-how-to-edit-the-web-pages)
- [git Reminder Reference](#git-reminder-reference)
- [Dual venues for our web pages](#dual-venues-for-our-web-pages)
- [Let's Encrypt for GetLab](#lets-encrypt-for-getlab)
- [Installing Let's Encrypt on our second venue](#installing-lets-encrypt-on-our-second-venue)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## GitLab CI

This project's static pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

_Note: This project expects all of the HTML files are in or below the
`public/` directory._

---

## Information about the Web Pages

- The web pages are accessible as
  http://digitalsats.gitlab.io/digitalsatswebpages
- The web pages project source is accessible as
  https://gitlab.com/digitalsats/digitalsatswebpages
- The web pages git source (for clone purposes) is obtained via
  ```git clone git@gitlab.com/digitalsats/digitalsatswebpages.git```
- For general information about GitLab web pages, see
  https://pages.gitlab.io and the official documentation
  https://docs.gitlab.com/ce/user/project/pages/
- The web pages project was originally created as a "fork" of the
  "plain-HTML" sample project. See
  https://pages.gitlab.io/plain-html
- For instructions on forking a project, see
  https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_forked_sample_project.html
- To look at the pipelines (CI) and to see the logs for the publishing of
  the pages see
  https://gitlab.com/digitalsats/digitalsatswebpages/-/pipelines

## Example of How to Edit the Web Pages

1. Obtain git.

   - Windows: Install Git for Windows at https://gitforwindows.org

   - MacOS: Type ```brew install git in terminal```

   - Debian: Type ```sudo apt-get install git-all```

   - Red Hat: Type ```sudo yum install git-all```

2. Configure your GitLab SSH key (if you haven't already done this).
   Follow the instructions at https://docs.gitlab.com/ce/ssh/README.html

3. Clone the repo to local storage.
   You will have new directory under the current working directory
   named "digitalsatswebpages"

   ```
   git clone https://gitlab.com/digitalsats/digitalsatswebpages

   _or_

   git clone git@gitlab.com:digitalsats/digitalsatswebpages.git
   ```

4. Copy/modify your new files and directories to the local folder
   just created (digitalsatswebpages).

   ```
   cd digitalsatswebpages

   cp -r _other-files-and-directories_ .
   ```

   _Now edit your files and directories in place..._

5. In order to check in your edits, you will have to _add_ them via
   git. You've probably forgotten which files you've changed, so get a
   list of changed files via

   ```
   cd digitalsatswebpages

   git status
   ```

6. Select which files you are interested in updating in the
   repository with the _add_ command.

   ```
   git add _filename_
   ```

   Do the above for each file and directory.

   By the way, if you've made a bunch of edits and want to temporarily
   suspend the checkin of several of them, then use the ```git
   stash``` command.

7. Commit previously staged changes (-m is followed by a comment
   describing what was done).

   ```
   git commit -m "Added new files and directories"
   ```

8. Send the changes to the remote repository.
   _Note: GitLab may take several minutes to update the repository and
          then automatically run the CI (continuout integration)
          pipeline to publish for the web._

   ```
   git push
   ```

9. The landing page is ```digitalsatswebpages/public/index.html```
   and the overall style page is ```digitalsatswebpages/public/style.css```

10. In addition to the HTML and CSS web pages, you will be concerned
   with a YAML file (```digitalsatswebpages/.gitlab-cl.yml```) and a
   Markdown file (```digitalsatswebpages/README.md```) which contain
   information about how to run CI and other support utilities.

11. Finally, git can be instructed to ignore certain files so you can
  keep such things as TAGS files, temporary output and backup edits
  around without them showing up in status. Look at
  ```digitalsatswebpages/.gitignore``

## git Reminder Reference

- Obtain a local copy of the web pages:

  ```git clone https://gitlab.com/digitalsats/digitalsatswebpages```

- Get current changes beyond last clone:

  ```git pull```

- Add a file:

  ```git add``` _file_

- Remove a file:

  ```git rm``` _file_

- Remove a file or directory recursively:

  ```git rm -f``` _dir_

- Restore a file accidentally deleted:

  ```git checkout``` _file_

- See where we are:

   ```git status```

- Check changes:

  ```git diff``` _file_

- Put changes aside temporarily while testing:

  ```git stash```

- Check what is stashed:

  ```git stash list```

- Put the stashed changes back in working dir:

  ```git stash pop```

- Commit changes:

  ```git commit -m "Changed things"```

- Put all committed changes into repository:

  ```git push```

## Dual venues for our web pages

We use the [https://digitalsats.gitlab.io/digitalsatswebpages](GitLab)
CI/CD automatically generated web pages for the project as our test
and staging area. When we are satisfied that the pages are working
correctly, we copy them to our outside server
[https://buildasat.com].

### Web site copy procedure

Here is the web site copy procedure:

1. Log into the second venue site. As of this writing, we are using
buildasat.com (a.k.a. ubuntu-s-1vcpu-1gb-nyc1-01).
You will need credentials with sufficient authority to overwrite the
Apache web page area on the machine.

2. Edit the following Bash script to change REMOTE_USER to be your
account on the second venue site (replacing
PUT-YOUR-LOCAL-USER-ID-HERE). Use a standard text editor (e.g. nano,
emacs, vi) and not a word processor (e.g. MS Word). You may already
find the script on the site under the name *update-buildasat*.

```bash
#!/bin/bash

GITLAB_HOST=digitalsats.gitlab.io
GITLAB_PAGES="digitalsatswebpages"

REMOTE_USER=PUT-YOUR-LOCAL-USER-ID-HERE
REMOTE_HOSTNAME="ubuntu-s-1vcpu-1gb-nyc1-01"
REMOTE_HOST=buildasat.com
REMOTE_PAGES=/var/www/${REMOTE_HOST}

STAGING_DIR=/tmp
STAGING_AREA=/tmp/${GITLAB_HOST}

APACHE_RESTART="apachectl restart"

if [[ "${HOSTNAME}"x != "${REMOTE_HOSTNAME}"x ]]; then
   echo "First, please 'ssh ${REMOTE_USER}@${REMOTE_HOST}'"
else
   pushd ${STAGING_DIR} >/dev/null 2>&1 || exit
   # Clear any lefovers from a previous run.
   rm -rf ${STAGING_AREA}/${GITLAB_HOST}
   # Copy the web pages from the GibLab web server.
   wget -rkv -l 99 -U firefox https://${GITLAB_HOST}/${GITLAB_PAGES}/
   cd ${STAGING_DIR}/${GITLAB_HOST}/${GITLAB_PAGES}/
   # Copy the web pages to the local Apache web directory.
   echo "Prompting for password in order to sudo copy to Apache web pages directory"
   sudo cp -r * /var/www/buildasat.com/
   # Remove the temporary copy.
   rm -rf ${STAGING_DIR}/${GITLAB_HOST}
   # Restart Apache to guarantee fresh contents will be visible.
   sudo ${APACHE_RESTART}
   echo "You may have to clear your browser cache in order to see updates!"
   popd >/dev/null 2>&1 || exit
fi
```

3. Run the Bash script with privilege.

```bash
sudo update-buildasat
```
4. You should see the web pages copy proceeding and it should take
under a minute. You may have to clear your browser cache in order to
see updates.

5. Please note that the ability to show https pages (encrypted) rather
than just http: (unencrypted) is automatically provided for GitLab web
pages. However this is not necessary the case for a second venue for
web pages. It wasn't the case for us. Therefore, we use Let's Encrypt
to provide encryption. The following section gives details for
enabling Let's Encrypt for your outside web pages.


## Let's Encrypt for GetLab

*Information as of 9/16/2022*

1. GitLab Pages (How they serve content; pages-root; config.json; ...)
   - https://gitlab.com/gitlab-org/gitlab-pages/blob/master/README.md
   - https://gitlab.com/gitlab-org/gitlab-pages

2. GitLab Pages administration - Troubleshooting Let's Encrypt certificates
   - https://docs.gitlab.com/ee/administration/pages/index.html

3. Tutorial: Securing your GitLab Pages with TLS and Let's Encrypt
   - https://about.gitlab.com/blog/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/

4. Secure GitLab Pages with Let's Encrypt Certificate - certbot
   - https://blog.howar31.com/lets-encrypt-ssl-gitlab-pages/#modes

5. GitLab Pages integration with Let's Encrypt
   - https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html

6. Automatic Letsencrypt SSL for Gitlab and GitLab Pages
   - https://swas.io/blog/automatic-letsencrypt-gitlab-pages/

7. Let's Encrypt - HTTPS add to GitLab Pages site
   - https://flinhong.github.io/front-end/lets-encrypt-with-gitlab-pages/

8. How do I use let's encrypt with gitlab?
   - https://stackoverflow.com/questions/34189199/how-do-i-use-let-s-encrypt-with-gitlab

9. Certbot
   - https://blog.howar31.com/lets-encrypt-ssl-gitlab-pages/#environment-requirements
   - https://eff-certbot.readthedocs.io/en/stable/install.html
   - https://certbot.eff.org/instructions?ws=apache&os=ubuntufocal
   - https://eff-certbot.readthedocs.io/en/stable/install.html

10.More Let's Encrypt information
   - https://duckduckgo.com/?q=gitlab+web+pages+letsencrypt&t=brave&ia=web


## Installing Let's Encrypt on our second venue

*Information as of 1/17/2022*

Here is a blow-by-blow for how we set up a second venue for our web
pages. Note that we're using a Digital Ocean machine with domain name
BuildASat.com and, of course, your will have to adjust the
instructions for your specific machine.

1. Install LAMP on your host.
   Follow the LAMP install instructions at
   https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-20-04

   Note: Set your web site domain as *name* and not *name.com*. For
   instance, *buildasat* instead of *buildasat.com*

2. Copy your GitLab web pages to this machine.

   Note: Change *myname* to your login name below and change the
   domain name and GitLab names appropriately too.

   ssh *myname*@buildasat.com
   wget -rkv -l 99 https://digitalsats.gitlab.io/digitalsatswebpages
   cp -r digitalsats.gitlab.io/digitalsatswebpages/ /var/www/DigitalSats/

3. Install Let's Encrypt
   Follow the Let's Encrypt instructions at
   https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-20-04

4. Obtain an SSL Certificate

   Assuming you are still logged onto your machine...

   ```bash
   sudo certbot --apache
   ```

   You may see something like the following:

   ```bash
   Domain: www.digitalsats.com
   Type:   unauthorized
   Detail: Invalid response from
   http://www.digitalsats.com/.well-known/acme-challenge/bQVuYmYOaUij5qx6Aw0v07b1sfFuQ3KsT4vNGnqrt0k
   [192.168.20.3]: 404
   ```

   To fix these errors, please make sure that your domain name was
   entered correctly and the DNS A/AAAA record(s) for that domain
   contain(s) the right IP address.

   ```bash
   myname@ubuntu-s-1vcpu-1gb-nyc1-01:~$
   ```

   You should receive email from certbot. Remember to save the
   information for later renewal and/or future edits.

   Okay, the above skips over a lot of details. However, the details
   keep changing. So just follow the prompts and do your best.

5. Congratulations! You have successfully enabled https://buildasat.com and
   https://www.buildasat.com

   ```
   You should test your configuration at:
   https://www.ssllabs.com/ssltest/analyze.html?d=buildasat.com
   https://www.ssllabs.com/ssltest/analyze.html?d=www.buildasat.com

   IMPORTANT NOTES

    - Congratulations! Your certificate and chain have been saved at:
      /etc/letsencrypt/live/buildasat.com/fullchain.pem
      Your key file has been saved at:
      /etc/letsencrypt/live/buildasat.com/privkey.pem
      Your cert will expire on 2022-04-18. To obtain a new or tweaked
      version of this certificate in the future, simply run certbot again
      with th e"certonly" option. To non-interactiely renew *all* of
      your certificates, run "certbot renew"
    - If you like Certbot, please consider supporting our work by:

      Donating to ISRB / Let's Encrypt: https://letsencrypt.org/donate
      Donatin to EFF:                   https://eff.org/donate-le
   ```

6. Auto-update

   Follow the auto-update at

   https://linuxize.com/post/secure-apache-with-let-s-encrypt-on-ubuntu-20-04/

   Once the certificate is renewed we also have to reload the Apache
   service. Append --renew-hok "systemctl reload apache2" to the
   /etc/cron.d/certbot file so that it looks like the following:

   ```bash
   sudo nano /etc/cron.d/certbot
      0 */12 * * * root test -x /usr/bincertbot -a \! -d \
      /run/systemd/system && perl -e 'sleep int(rand(3600))' && \
      certbot -q renew --renew-hook "systemctl reload apache2"
   ```

   Test with

   ```bash
   sudo certbot renew --dry-run
   ```
