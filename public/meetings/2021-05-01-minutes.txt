May 1, 2021 10:41:30 AM (Saturday)
   Saturday design meeting
   Tung, Harry, Jim, Don

   Tung liked SMAD.

   Meetup Page
      https://www.meetup.com/smallsats

   GitLab Markdown
      https://docs.gitlab.com/ee/user/markdown.html
      gfm-mode for emacs (part of markdown-mode.el)

   10am-2:45pm (2 hours 45 minutes!)
