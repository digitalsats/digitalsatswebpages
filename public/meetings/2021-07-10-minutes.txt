July 10, 2021 10:19:06 AM
   https://meet.google.com/sjc-uzdm-fao

   R2020a - Python interface, solar wind computations, new 6DOF support

   6DOF - Six degrees of Freedom
   ENTD - NASA Evolutionary Mission Trajectory Generator
          https://software.nasa.gov/software/GSC-18459-1
   SNOP - Stanford Network Analysis Platform
          (consider an open-source SNOT alternative)
   SRP - Solar Radiation Pressure

   Jonathan:
      innersourcing - hardware open source
   Jonathan just moved to Arkansas last week...

   Separate script landing page, demo


   Octave is the GNU open source replacement for MATLAB
      Similar but not perfect replacement.
