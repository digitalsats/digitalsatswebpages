Minutes of August 16, 2021 6:30 PM
   Zoom recruitment meeting
      Harry Goldschmitt
      John Kastner
      Don Black
      Jim Stephens
      Andrew Emmons
      Daniel Darrenn Beatty - joined 7:04pm (was with group before)

After a number of Zoom issues, the meeting started at 6:46pm.

Don started the introductions. Then Harry, John, Jim, Andrew.

Andrew is currently with a small start-up efficiency of traffic
signals. Recently came from North County, San Diego. Background as a
software engineer. Before, he used to work with small drones.

6:50pm Don showed his PowerPoint presentation. Don will mail it to the
group and put it online.

Don encouraged people to join the SmallSats mailing list.

7:18pm Harry showed his presentation on GitLab, and his Ubuntu GMAT
OVA.

7:30pm John gave a demo of GMAT showing an orbit and then doing a burn.

7:43pm Dan introduced himself. He lives in the desert. He's a space
enthusiast with a computer background. He expressed an interest in
GMAT/Python on Pi and/or Arm small processors and that started a
conversation about GMAT's simulation capability and not meant for
embedding.

7:50pm Jim talked about the video of GMAT simulations of the Apollo 11
Eagle's current state (whether still orbiting the moon).

7:58pm In response to Andrew's query about a group roadmap, Don showed
the group's old GMAT attitude development plan with the attitude data
flow diagram. Don's computer glitched out so...

8:06 Jim showed the old Attitude Module plan (Google Docs PDF) and Don
talked about it. While the slides are out of date, they at least
offered a start of conversations about possibilities for the group.

8:30pm Declared the meeting over.
