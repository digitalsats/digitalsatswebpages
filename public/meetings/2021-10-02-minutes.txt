Minutes of Saturday, October 2, 2021 10:05:25 AM
   Don Black
   John Kastner
   Harry Goldschmitt
   Jim Stephens
   Jinan Alshaikhhussain (B.S. Computer Science, M.S. Engineering)
      works for Schlumberger - field engineer
   Russell Lenahan (B.S. EE, M.S. CS) from Madison, Wisconsin
      works on medical records
   Daniel Beatty from Ridgecrest, California
   Tung Nyuyen
   Remi Cornwall from England

- 10:05am Introductions
- Don talked about groups:
   DevOps     - Tools: Don, Harry, Jim
   Python     - Software:
   Trajectory - AE types:
- Don talked about GitLab "issues"
  People are to find an issue and start working on expanding the
  detail and then addressing it via research, coding, etc.
- Don will "grant" access to those who can't see GitLab issues.
- Don solicited help in improving the details of the issues.
- 10:24am Daniel Beatty joined
- 10:44am Tung Nguyen joined
- Russell requested a blow-by-blow set of instructions for
  download/upload of GMAT script and Python code into/out of the
  Ubuntu environment.
- 10:45am Remi Cornwall joined (6:45pm his time in England)
- 11:10am Discussion on patents of orbits.
- 11:16am Discussion of China's kinetic weapon bringing down a US
  weather satellite and the US Space Force.
- 11:25am Don's connection started producing excessive feedback and
  Don claimed that video was frozen.
- 11:35am Adjourned
