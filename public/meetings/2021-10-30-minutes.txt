Meeting of Saturday, October 30, 2021
   Don Black
   Harry Goldschmitt
   John Kastner
   Jonathan Nacionales
   Russell Lenahan
   Sindu Raju
   Martin Martinez
   Daniel Beatty

10:00am People started connections
10:05am Don said he would attempt to follow the published agenda.
10:10am Slack and Discord set up. Don started with Slack but found
        he doesn't like Slack. Hypothesis: Slack for software,
        Discord for gaming.
10:13am Jonathan claims "Team" is the communication tool of the
        month. However, it might require a "corporate" context.
10:15am Discord was "chosen" for the group. Russell moved. Harry
        seconded. Unanimous Aye. Followed by some discussion of
        Roberts Rules of Order.
10:20am John said he'd like Octave included in the standard OVA
        collection of software. Although not required by GMAT, MATLAB
        is used for orbit optimization among other things. Current
        pricing for MATLAB is $169 per person per year.
10:26am Martin Martinez joined.
10:27am Harry said Darrell Conway set up a MATLAB plugin. So it might
        be easier than we originally thought to supply Octave.
10:29am Harry "volunteered" to put Octave in the OVA. Current OVA is
        4Meg so Octave might substantially increase the size.
10:30am Don put in the agenda that adding the Atmosphere and Solar
        Weather models would be useful. Harry volunteered to put it
        in.
10:31am Harry will change the wrong pointers in the current OVA. In
        the binary file the pointers work. The problem is in the
        source files. Harry will fix and publish what he did.
10:31am Daniel Beatty joined.
10:33am Harry will set a date for the next OVA release. Harry
        suggested CI/CD tools and tests for OVA updates.
10:34am Russell suggested a more convenient updater for handling
        pointers for Python. He said new Python releases can cause
        problems.
10:38am Suggestions for a more automated OVA creation and packaging
        procedure. Or at least a check and verify.
10:41am Russell says that any Python added to call GMAT will require
        hand inclusion of the Python routines to the GMAT directories.
        Russell said he'd look into a more automated procedure for
        this. We'd need a standard environment available to CI/CD and
        YAML for testing.
10:48am Harry estimated three weeks to create a new OVA with the
        discussed additions. Don wanted to establish a standard
        release and suggested the 2nd Saturday meeting of the month.
        Harry requested that we add issues to the GMAT box
        repository. Harry suggested releases on the 1st Monday of the
        month starting in December.
10:53am Sindu volunteered to attempt to sync the group's procedures
        more closely to those used by NASA for project programs.
        Spaceflight project management structures (high
        standards). Tech addition management structures (lower
        standards).
10:56am Sindu dropped off. Incidentally called from a boat and we
        heard seals in the background.
11:01am Don would like to create a user's group for GMAT/script
        development. "GMAT Users Group"
11:07am Jonathan dropped off.
11:08am Confusion about Python Script versus GMAT Script. GMAT calling
        Python ("Python Interface") versus Python calling GMAT
        ("Python API").
11:17am Daniel Beatty noticed links on GMAT/Python:
        http://gmat.sourceforge.net/docs/R2020a/api/userguide/GettingStarted.html
        https://1u2ucy2936go2zwfaa1rqhvy-wpengine.netdna-ssl.com/wp-content/uploads/GMAT_API.pdf
11:17am Discussion of the group's focus. Not all parts of GMAT work
        (e.g. configure script wasn't included originally in the
        distribution). Open Frames project (on GitHub) was paid ~$800K
        to be included/applied with open source GMAT. (See SBIR
        program).
11:26am Russell has some GMAT Python "gotchas" that he will
        document. Suggest creation of an accurate and correct Wiki for
        creation and running of GMAT with Python. Perhaps the group's
        project and focus should be the instructions and tools we
        develop instead of a specific model (e.g. attitude module).
11:31am Don suggests looking at http://teledutainment.com/GMAT.html
11:32am Don would like to have a library of scripts that actually run
        on GMAT.
11:38am Russell was volunteered to be point person for GMAT scripting
        on Discord. He will set up a "Discord GMAT Script" group.
11:54am Suggestion that we need to specify the Python version in the
        OVA. Updating the Python after building the OVA is ill-advised
        (breaks links). Harry's current OVA Python version is "3.8.10"
12:11pm Proposed to build a GMAT Dashboard for Python
12:34pm Still creating and adding groups to Discord's DigitalSats group
12:35pm Next SDR on Saturday 11/13/21 at 10:00am
        Agendas: GitLab vs. GitHub, and more
12:37pm Meeting adjourned

Note: Here is the information for joining Discord:
      URL:
         https://discord.com
      Invitation:
         https://discord.gg/Mkv3eJcgr6
      Group:
         DigitalSats
