Agenda of Monday, November 8, 2021 6:30pm

1 Don: Introduction and Brief History
1.1 Hardware ==> Software MVP
1.1.1 Found NASA GMAT for Trajectory Simulations
1.2 Software ==> Script Library MVP
1.2.1 Seeking help from experienced Script Writers.
1.3 Seeking Software Designers/Developers, Physicists, & AE's   for:
1.3.1 AE Mission Planners,
1.3.2 AE Scripters,
1.3.3 Python Developers,
1.3.4 DevOps
1.3.5 Officers/Administrators
2 Don: Onboarding - How to
2.1 Join SatSw list. mailto:SatSW-join@mail.SoftCafe.net
2.2 Send resume to: career@digitalsats.com
2.3 Join SmallSats list: mailto:SmallSats-join@mail.SoftCafe.net
2.4 Join Discord. Ask for access to channels.   Click on channel?
2.5 Join Google Drive
2.5.1 Send us your Google Drive username (dblack@digitalsats.com)
2.5.2 List of directory structures and shared documents
2.5.3 Examine and share editing of docs
2.5.4 Download large files (e.g. OVA)
2.6 Join GitLab
2.6.1 Send us your GitLab username (email address)
2.6.2 Examine Outstanding tasks (issues)
3 Harry: DevOps - How to
3.1 Select and Perform New Member Tasks
3.1.1 Download and Install our .OVA file for Software Developers
3.1.2 Script writers can use GMAT binaries from SourceForge, instead.
3.1.2.1 How to download & install from SourceForge
3.1.2.1.1 Win 10
3.1.2.1.2 MacOS
3.1.2.1.3 Ubuntu 20.04
3.1.2.1.4 IBM RedHat ...
3.2 Begin Contributing
3.2.1 Examine outstanding issues via GitLab
3.2.2 Select issues/teams of interest via GitLab
3.2.3 Self-assign to issue(s) and/or team(s) via GitLab & Discord
3.2.4 Join Discord channel(s) to announce your intent to participate
