Meeting of Monday, November 8, 2021
   Don Black, PhD [Uses Windows]
   John Kastner, PhD [Uses Mac, Linux]
   Paul B. Huter (Graduated 2006, now contracting AE stuff and computer)
                 [Uses Windows10]
   Daniela Solis (Junior, Mechanical Engineering, transferred from Business)
                 More hands on in EE. Interested in Python. Currently
                 at Cal State LA (in the valley), born and raised in
                 Sun Valley (North East of Van Nuys, North West of
                 Burbank), taking classes in differential equations.
                 [Uses Windows10]
   Ajay (initially no video or audio - Zoom showed as ajayv)
   Martin Martinez [Uses Linux, has Windows10]
   Daniel Beatty, PhD [Uses Mac]
   Eugene M. Murray (Zoom showed as E2Murray)
   Leif Hendricks

6:36pm Pre-meeting chat.
       Daniela just built a Lego model of the Saturn 5.
       Paul has framed posters in his room of gravity wells, the solar
       system, ...
6:42pm Don's introductions. Started group two years ago.
6:44pm Martin joined.
6:45pm AJay's camera and video turned on.
6:47pm Don's intro continues.
       Discussed Discord channels used by the group.
       Discussed GMAT scripts.
6:57pm Don said some had complained about long meeting and so he is
       starting a "happy hour" group for long chats,
       brainstorming and whatever at 6:30pm on Wednesday.
6:59pm Agenda #3, kinds of people the group is seeking.
7:05pm John put up a non-expiring invitation link to the DigitalSats
       Discord group: https://discord.gg/Mkv3eJcgr6
       Paul and Daniela tried it and it appeared to succeed.
7:08pm Don sent out (in the chat window)
       mailto:SatSW-join@mail.SoftCafe.net and career@digitalsats.com
       links for those who choose to join the group. And for the
       Google Drive, mail to: dblack@digitalsats.com
7:11pm Daniel joined.
7:12pm AJay dropped off.
7:14pm Don continued with how to get started (New , download GMAT, run some
       tutorials, ...
7:24pm Don pointed the group to http://digitalsats.com and click on
       "Learn More"
7:26pm Eugene (E2Murray) unsuccessfully tried to connect.
7:32pm Don described a group initial idea to use plasma engine for a
       space pickup truck which morphed into small satellites.
7:34pm Eugene tried again to connect. Ended with two black silent
       screens.
7:37pm Leif connecting. Just a black screen with his name on it.
7:48pm Leif fixed his video and audio. He said he has a background in
       remote sensing and is interested in small satellites. Has used
       C and C++.
7:51pm Don described the idea of the group collecting, indexing and
       curating a library of GMAT scripts for open-source use.
7:55pm Eugene's camera never worked. But he did talk with
       us. Background in safety programming and optimization for
       NASA. Has used C and C++. Did work on databases and
       Fortran. Worked on Challenger post analysis and Direct-TV
       problems. Graduating from Cal State Fullerton.
8:00pm Don CFS and CFE as an example of going beyond GMAT simulation
       and actually plugging hardware in the loop.
8:01pm AJay background in application programming. Examples in FedEx
       and school busses processing messages to summarize and generate
       solutions.
8:04pm Eugene safety reliability studies before MATLAB. Lots of
       spreadsheet analyses.
8:06pm Paul did post Columbia shuttle safety program. Then worked on
       missile defense in Huntsville, Alabama. Has used STK
       (tinkering). Some next-gen GPS. Now contract consulting.
8:08pm Eugene worked operational performance systems. Suggested
       starting with a small subset of GMAT.  Would like a small model
       with some specific samples for focus.
8:16pm Discussion of magnetorquers. See
       http://sea.softcafe.net/digitalsats/vendors.html
8:20pm Don showed his Twitter account with Space News.
8:21pm Don panned through the group's GitLab "Issues" list.
8:22pm Don mentioned that spacecraft de-tumble capability would be a
       good task.
8:26pm Don showed his TelEdutainment site,
       http:/teledutainment.com/GMAT.html
8:28pm In response to AJay's query about where to find all of the
       things Don has discussed, Don showed his Digital Astronautics
       web site again (click on Learn More). Don showed the download
       site for the OVA http:/digitalsats/download
8:32pm http://test.digitalsats.com/2021/08/17/downloading-the-gmat-ova-from-google-drive/
8:35pm Eugene said he worked on NASA OMIS system to handle
       modifications for shuttle missions (functional management and
       engineering)
8:50pm Don said his Wednesday 6:30pm would be a weekly Zoom
       chat/brainstorm session.
8:54pm Meeting terminated with some people hanging on to chat.
