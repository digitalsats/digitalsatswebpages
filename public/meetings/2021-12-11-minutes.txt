Meeting of Saturday, December 11, 2021
   Don Black
   John Kastner
   Harry Goldschmitt
   Russell Lenahan
   Jim Stephens
   Ajay Vijayasarathy

10:00am Meeting started. Don's video wasn't working.
10:00am Informal discussion of missions by Jim
10:10am Don's video is now working.
10:11am Russell presented his proposal for his GMAT script group
        Propose Goals:
        1) Reverse engineering of GMAT related to attitude control
           What is SPICE?
        2) Need to specify what we need to do for attitude.
10:24am Discussion of GMAT interactive graphics versus orbit solutions
10:25am Discussion of orbit timings, graphical presentation timings,
        accuracy of curve solutions, etc.
10:35am Russell would like a clear description, diagrams, charts, etc.
        of what GMAT does internally in solvers, propagators and graph
        generators.
        Current GMAT System Design description:
        http://gmat.sourceforge.net/docs/R2020a/api/design/Design.html
10:42am Harry mentioned C++ plug-ins (see Darrell Conway's work) for
        hooking into GMAT our unique propagators. C++ would have more
        speed than Python plug-ins.
10:54am Discussion of new group, GMAT-Physics group (Angular Momentum
        processing), proposed by Don.
11:03am Russell proposed that the more detailed physics might be
        better postponed until later.
11:08am Don's MVP proposal:
        CubeSat with Hall-effect thruster to get from
        ISS (~422km=262mi; 90 minute orbit) to
        GPS (~20,180km=12,540mi; 12 hour orbit).
11:32am Vijay said he'd look into Let's Encrypt for the web site.
        He gave an example of his work: Bootstrap css
        https://themes.getbootstrap.com/
        https://getbootstrap.com/
        Digital Ocean droplets access:
           https://cloud.digitalocean.com/droplets?i=786321&preserveScrollPosition=false
        Digital Ocean console access:
           https://cloud.digitalocean.com/droplets/271285391/terminal/ui/?os_user=root
11:34am December 15 deadline for a SBIR grant (claims Sindu Raju).
        First requirement would be the names and credentials of
        individuals. Needs a data dictionary for all sensor and
        actuator input and output.

        Correction and expansion by Sindu:
           December 15th is not a deadline for an SBIR. December
           15th Is the timeframe to determine a sensor/actuaoter to
           start defining the Digital Sats module for writing the
           SBIR.

           The  SBIR is a federal government research grant form
           the National Science Foundation.  It is called Amerca’s
           seed fund. https://seedfund.nsf.gov/

           We can propose a project.. to do this we need to define
           an MVP. I think what we are trying to do is to develop
           an attitude module that would interface with GMAT and
           other software.  Once we define an actual product we can
           submit to NSF for funding. We need to set timelines for
           making decisions around the sending of an SBIR.

           It seems like we have determined:
              CubeSat with Hall-effect thruster to get from ISS
              (~422km=262mi; 90 minute orbit) to GPS
              (~20,180km=12,540mi; 12 hour orbit).
11:50am Harry is working on an updated OVA for GMAT work.
        New GMAT is schedule to come out in 2022.
        R2020a is reportedly the last version to use wxWidgets. The
        next release is scheduled to use OpenFrameworks.
12:10pm End of meeting
Next Saturday meeting on January 8, 2022 (skipping Christmas day)
Coming Monday meeting (recruitment) on December 13, 2021
