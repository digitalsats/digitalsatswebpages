Meeting of Saturday, April 2, 2022 10:00 AM

Attendees:
   Don Black, Ph.D.
   Harry Goldschmitt
   John Kastner, Ph.D.

10:00am Meeting started
10:01am Discussion of UPS units to save work during power problems.
10:10am Discussion of GMAT development user groups on SourceForge
        along with recent activity on those groups.
10:20am Discussion of Government grants.
10:40am Discussion of browsers and security. Harry uses Bitwarden.
10:50am RFP Status and government registration
10:55am John is still working on QA hooks for using script to
        regression test Vagrant/GMAT/Ubuntu builds.
11:02am Harry is still working on VNC for GMAT Ubuntu usage. RDP
        vs. VNC. Problems with GNOME.
11:00am Meeting ended
