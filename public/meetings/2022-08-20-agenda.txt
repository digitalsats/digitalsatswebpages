August 20, 2022 Informal Saturday Discussion

DigitalSats Folk - Reminder:
I)  I will host an Informal 10:00AM Brainstorm Discussion on
    Simulation Fundamentals, Tomorrow (Saturday);
II) Our usual Saturday PDR is now on held on Alternate Wednesdays at 6:30PM.
    Our next Program Design Review will be a week from Wednesday on
8/31/22.


I) My Proposed Saturday Agenda is a casual discussion and exchange of
ideas about:
1) Number systems: Float, Integer, and BCD, with regard to Resolution,
Precision and Accuracy.
2) Computational verses Analytical Solutions
3) Monte Carlo Techniques, especially Gaussian Distributions and the
Central Limit Theorem
4) Sensor precision scales as Sqrt(n).

We'll combine our knowledge and discuss these topics and to shed a
little light on them.

Dr. Don V Black
