August 31, 2022 Wednesday

Minutes for 8/31/22
   John Wright - hosting
   Don Black, Ph.D.
   John Kastner, Ph.D.
   Harry Goldschmitt

6:30pm Meeting started.
6:30pm John K. described his new projects gmat-collected-scripts and
       gmat-qa-scripts. These are to automatically test scripts. The
       next steps are to automatically instrument scripts so that
       their runs are able to be monitored.
6:35pm Harry described his attempts to automatically bring up a
       Digital Ocean virtual Ubuntu console in graphics mode.
       Hot To Use GUI in DigitalOcean Droplet? - 2 Easy Steps
       by Kaustubh Kulkami
       See: YouTube's https://youtube.com/watch?v=1sdGux7Bpxk
       Digital Ocean has a new feature
       "Enable the new Droplet Console for native-like terminal access
       to your Droplet from your browser."
7:11pm Harry was distracted by visitors but will continue to work on
       Jupyter and Octave again.
7:15pm Don has been monitoring the new interested users and has been
       rejecting High Schoolers. Don has been urging updates for the
       group web pages. John has been implementing those updates.
       Don is working on the dimensions talk for this coming Saturday.
       Don is still working with Jonathan.
       Discussion of hyper accurate time for position analysis. GPS
       clock is accurate to within 40ns (John W.). Don is talking
       about 10^-18 second.
7:32pm John W. talked about his ISS Tracker GMAT script code. He found
       errors depending on West of Grenwich versus East for unknown
       reasons. John will be comparing with Colin Helms' code. Still
       have some issues.
7:40pm Detailed discussion of the Alfano code while sharing John's
       screen. John's code is close but about a percent or two
       different from Colin's code concerning the amount of fuel
       used.
          John's run: 21 sec; left with 279.6kg fuel
          Colin's run: 3 sec; left with 296.4kg fuel
       Using elliptical polynomials for the analysis.
8:26pm Don would like a new goal to add delta-tau in the trajectory
       table (need a table for strength of gravity at a position).
8:31pm Meeting adjourned.
