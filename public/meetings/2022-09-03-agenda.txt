September 3, 2022 Informal Saturday Discussion

Saturday (9/3/22) at 10:00am, I will casually discuss Extra Dimensions.
This is required to explore physics in higher dimensions, such as 5D
General Relativity, for example.

I will discuss higher dimensions beyond the usual three dimensions we
see around us.  We will discuss basis vectors, open vs closed
dimensions, infinite vs finite bounds, flat vs curved spaces, the
metric, transversality, co-dimensions, hyperplanes, extrusion,
tessellation, foliation, and d-branes, as well as constructing and
interactively viewing hyper-surfaces and higher dimensions.

Or as much as I can fit into 90 minutes, leaving time for Q&A.

Future talks will include:
Gravity 101: Time vs 5D GR vs Dimplons vs Solitons
Time 101: Origin of Gravity; Origin of The Big Bang;
Origins 101 : Origin of Mass, of Inertia, of Energy, of Big Bang
Pre-Bang 101 : The Big Birth; The Big Bounce; Cyclic; Penrose'; No Boundary

I hope to see you folk on Saturday at 10:00am.
- Dr Don
