September 28, 2022 6:30 PM Agenda

   Projects:
      1) Developer Standups
      2) Review of Current Goals
      3) Introduction of New Goals
      4) Deep dive into a particular path

   List of goals and assignees:
      1) Full Attitude Support in GMAT (John W.)
      2) Smaller Attitude Modules (e.g. Alfano) (John W.)
      3) Web Assembly version of GMAT (Artem)
      4) GMAT Script Store/Repo (John K.)
      5) Testing Scripts (John K.)
      6) Octave Integration into GMAT (Martin)
      7) White paper on Specs->Script tool (Don)
      8) Mission Implementation Wizard (open)
      9) dTau in spacecraft trajetory files (John W. and Don)
      10) Spacecraft Articulation (Daniel)
