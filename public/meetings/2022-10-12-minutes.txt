October 12, 2022 6:35:41 PM Minutes
   Don Black, Ph.D.
   Harry Goldschmitt
   John Wright
   John Kastner, Ph.D.

6:30pm Meeting started
6:30pm Harry is having trouble reestablishing full open source status
       to our GitLab projects. Harry will share details of his
       efforts.
       It's possible we might want to consider alternatives to GitLab
       (GitHub, Bitbucket, SourceForge, AWS CodeCommit, CircleCI,
       RhodeCode, Gitea, Gogs, Azure DevOps, Quire, Buddy.Works,
       Launchpad, ...).
6:54pm John W. has been extending his GMAT script plugins and extensions.
       He wrote a comments-in-code to Markdown documentation Python
       tool. He has continued to work on his Alfono attitude
       scripts. He hasn't fully integrated the finite burn with
       attitude yet. He has a question about what is the proper way to
       put finite burn together with attitude to produce an Alfono
       orbit change module easily understood and used to others.
7:17pm Harry asked about how NASA validates GMAT code. What non-GMAT
       tools do they use?
7:27pm Proposal to have Harry talk to Steve Hughes to ask
       questions. How do we verify our new Alfono orbit trasfer module
       code? What tools beside GMAT does NASA have available to us?
       Some question about who (Don or Harry) last contacted Steve.
       Hughes, Steven P. (GSFC-5950) <steven.p.hughes@nasa.gov>
7:45pm John K. extended his QA Python tool. He added more bullet
       proofing, error handling, and more test points. It now modifies
       each test script to add test points, runs the script and
       then compares the script's answers at those test points to
       known answers from previous runs.
7:48pm Don is still researching for a white paper. He's working
       on relativistic escape velocity computations. Discussion of
       LaGrange points with respect to this.
8:10pm Suggestion to add a new goal: 11) How do we validate new GMAT
       features as we add them.
8:15pm Discussion of goals.
8:20pm Discussion of how to shorten these meeting.s
8:25pm Meeting adjourned
