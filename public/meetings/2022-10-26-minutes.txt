October 26, 2022 6:35:41 PM Minutes
   Don Black, Ph.D.
   Harry Goldschmitt
   John Kastner, Ph.D.
   Martin Martinez

6:30pm Meeting started, John Kastner moderating for John Wright
6:31pm John: No progress on re-obtaining full GitLab open-source benefits
6:32pm Harry: New GMAT 2022a will be coming out soon.
6:34pm Martin joined
6:41pm Harry: GitLab personal GitLab license, "harry@hgac.com" with id:
       goldharv) expired so he created "harry+gitlab@hgac.com" but now
       does not have any access to the projects. He claims @goldharv
       and harry@hgac.com no longer work for GitLab.
6:50pm No response from Artem so his Web Assembly project has no
       progress. That goal should be dropped.
6:52pm No response from Daniel so his Spacecraft Articulation project
       has no progress.
6:56pm Martin: Been sick and had family visiting. Limited progress on
       Jupyter tutorial and Octave integration into GMAT.
7:00pm John: More improvements
7:11pm Don: No progress on white paper. Some work on dTau, escape
       velocity work, but no conclusions yet. Newtonian versus
       relativist space-time equations.
7:20pm Martin will look in his copy of Steven Weinberg's "Gravitation
       and Cosmology - Principles and Applications" to see if he can
       find some help for Don.
7:48pm Harry will be out of town Nov. 16 - Dec 1.
       Don will be out of town Nov. 10- 13.
       Martin will be unavailable Nov. 13-16.
7:49pm Nov. 9 will still be okay for the next meeting.
7:51pm Don needs an automatic onboarding procedure for new
       members. Need a "press a button" for new members to get started
       with GMAT.
8:00pm Discussion of how to validate a GMAT script. Compare with old
       mission data, or run CSPICE, or ... Perhaps ask NASA for
       availablity of some other tool.
8:05pm Meeting adjourned.
