November 9, 2022 6:30 PM Minutes
   John Wright
   Don Black, Ph.D.
   John Kastner, Ph.D.
   Martin Martinez

6:30pm Meeting started.
6:30pm Random discussions obstensibly while waiting for Margin
6:35pm John K's Zoom crashed (and did it again in a few minutes). John
       went to another machine.
6:40pm John K. described his efforts to use Octave to replace
       MATLAB. GMAT simply outputs an exception message and
       continues. The script will run to completion and the external
       Python can only parse the error messages to determine if the
       run was successful.
       Sample MATLAB error message when using OCTAVE as a replacement
       and running a GMAT script:
       Ex_MinFuelLunarCapture.script: **** ERROR **** Interpreter
       Exception: Cannot find LHS object named "SQP1" in line:
       " 108: GMAT SQP1.SourceType = MATLAB;"
6:45pm Martin joined.
6:50pm John continued to explain his difficulty running Octave called
       by GMAT scripts.
7:09pm Don will be losing his ACM-OC mailing lists (ACM wants them back)
       in a few weeks. This is his access to about 3K people.
       Don asked for more brainstorming on the project.
7:22pm Martin said he have Octave working. But his method was using
       Java to call the MATLAB interface (not calling MATLAB from
       GMAT).
7:31pm Martin again promised to write up something about how he
       interfaced in Octave to work with GMAT.
7:30pm Martin and Don discussed old field theory literature that they
       have. Martin referenced some work by Russians.
7:41pm Don and Martin continued to discuss Einstein's equations.
7:44pm John W. has been working on dTau code for General Relativity
       time dilation. Also, he had been trying to get his Alfono C++
       code to work with other types of orbits (e.g. non-polar). GMAT
       does not have the ability to pass variables into his special
       C++ code. Can only pass numbers inside square brackets. GMAT
       does not support function calls before the "begin mission"
       statement. GMAT can only read/write numbers. You can't
       arbitrarily call functions at any point. You can only pass
       string and numbers.
8:01pm John K. had to leave.
        Meeting adjourned some time later.
