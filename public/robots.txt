# -*- mode: html; -*-
# Time-stamp: "2021-07-20 13:44:16 kastner"
# Robots.txt file for /WWW/ (kc.softcafe.net)
# Disallow any robot access
# 05/02/18-DVB
User-agent: *
Dissallow: /@S
Dissallow: /bo
Dissallow: /cs
Dissallow: /ga
Dissallow: /im
Dissallow: /ro
